package com.company.homework15;

import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class JDBCTemplate {
    private final DataSource source;

    public <T> List<T> query(String query, Object[] params, Function<ResultSet, T> convertor) {
        try (Connection connection = source.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);

            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    statement.setObject(i + 1, params[i]);
                }
            }

            ResultSet resultSet = statement.executeQuery();

            List<T> list = new ArrayList<>();

            while (resultSet.next()) {
                list.add(convertor.apply(resultSet));
            }

            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return List.of();
        }
    }

    public void query(String query, Object[] params) {
        try (Connection connection = source.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);

            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    statement.setObject(i + 1, params[i]);
                }
            }

            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
