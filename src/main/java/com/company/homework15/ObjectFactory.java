package com.company.homework15;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ObjectFactory {
    private static Map<Class, Object> instances = new HashMap<>();

    static {
        instances.put(DataSource.class, createDataSource());
        instances.put(JDBCTemplate.class, new JDBCTemplate((DataSource) instances.get(DataSource.class)));
    }

    private static DataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/hw15");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("1234");
        return new HikariDataSource(hikariConfig);
    }

    public static synchronized <T> T getObject(Class<T> clazz) {
        if (!instances.containsKey(clazz)) {
            try {
                Object o = clazz.getConstructor().newInstance();
                instances.put(clazz, o);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return (T) instances.get(clazz);
    }
}
