package com.company.homework15;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class User {
    int id;
    String login;
    String password;
}
