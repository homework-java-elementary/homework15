package com.company.homework15;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = {"/menu", "/delete", "/post"}, name = "UserServlet")
public class UserServlet extends HttpServlet {
    private JDBCTemplate jdbcTemplate = ObjectFactory.getObject(JDBCTemplate.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/menu")) {
            List<User> users = jdbcTemplate.query(
                    "select * from users",
                    new Object[]{},
                    (r -> {
                        try {
                            return User.builder()
                                    .id(r.getInt("id"))
                                    .login(r.getString("login"))
                                    .password(r.getString("password"))
                                    .build();
                        } catch (SQLException throwables) {
                            return null;
                        }
                    }
                    )
            );
            req.setAttribute("users", users);
            getServletContext().getRequestDispatcher("/WEB-INF/views/main.jsp").forward(req, resp);
        } else {
            super.doGet(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/post")) {
            jdbcTemplate.query(
                    "insert into users(login, password) values(?,?)",
                    new Object[]{req.getParameter("login"), req.getParameter("password")}
            );

            resp.sendRedirect(req.getContextPath() + "/menu");
        } else if (req.getServletPath().equals("/delete")) {
            doDelete(req, resp);
        } else {
            super.doPost(req, resp);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        jdbcTemplate.query(
                "delete from users where id= ?;",
                new Object[]{Integer.parseInt(req.getParameter("id"))}
        );

        resp.sendRedirect(req.getContextPath() + "/menu");
    }
}
