<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="com.company.homework15.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <title>Users</title>
</head>
<body>

    <form action="<c:url value="/post"></c:url>" method="post">
        <p>Login<br>
        <input type="text" name="login"><br>
        Password<br>
        <input type="text" name="password"></p>
        <p><input type="submit" value="post"></p><br>
    </form>
    <form action="<c:url value="/delete"></c:url>" method="post">
        <p>Id<br>
        <input type="text" name="id"></p>
        <p><input type="submit" value="delete"></p><br>
    </form>

<table border="2">
    <tr>
       <td>Id</td>
       <td>Login</td>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.login}</td>
        </tr>
    </c:forEach>

</body>
</html>